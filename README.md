# Vocabularies

For maintenance and creation of vocabularies to be used in RS.

Currently contains:
- cwrc.ttl: The CWRC  Vocabulary (Vocabulary version of [CWRC Ontology](https://sparql.cwrc.ca/ontologies/cwrc.html))
- genre.ttl: The CWRC Genre Vocabulary (Vocabulary version of [CWRC Genre Ontology](https://sparql.cwrc.ca/ontologies/genre.html))
- ii.ttl: The CWRC Illness and Injury Vocabulary (Vocabulary version of [CWRC Illness and Injury Ontology](https://sparql.cwrc.ca/ontologies/ii.html))

## Namespaces
- cwrc.ttl --> http://id.lincsproject.ca/cwrc#
- genre.ttl --> http://id.lincsproject.ca/genre#
- ii.ttl --> http://id.lincsproject.ca/ii#


## Future Vocabularies
Future vocabularies should have the following namespace pattern `http://id.lincsproject.ca/{prefix}#` where `{prefix}` should be replaced with the agreed upon vocab prefix. Note the `http` not `https`.

## New Vocab Checklist
- [ ] Valid namespace URI?
- [ ] SKOS Vocabulary
    - [ ] Entities declared as `skos:Concept` or `skos:ConceptScheme` where applicable
- [ ] Entities have appropriate CIDOC-CRM typing (ex. `crm:E55_Type`,`crm:E28_Conceptual_Object` and `crm:P2_has_type`)
- [ ] No usage of [blank nodes](https://www.w3.org/TR/rdf11-concepts/#dfn-blank-node)
- [ ] File is less the 1 gb if possible
- [ ] Every entity has an English label? 
- [ ] (Eventually) Every entity has a French Label?
- [ ] File is syntax error free?
	- Try [Validata](https://www.w3.org/2015/03/ShExValidata/) or # [IDLab Turtle Validator](http://ttl.summerofcode.be/) or uploading to a triplestore
